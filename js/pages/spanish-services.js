//HANDLE PAGE LINKING  IN SPANISH SERVICE PAGE
$('.page-linking').on('click', function(){
	openPage($(this).data('pages'));
});

//TOGGLE SPANISH
$('.translate-toggle-spanish').on('click', function(){
	$(this).removeClass('view-cta');
	$('.translate-toggle-english').addClass('view-cta');

	$('.english').removeClass('view');
	$('.spanish').addClass('view');
});

//TOGGLE ENGLISH
$('.translate-toggle-english').on('click', function(){
	$(this).removeClass('view-cta');
	$('.translate-toggle-spanish').addClass('view-cta');

	$('.spanish').removeClass('view');
	$('.english').addClass('view');
});