// HANDLE BACK TO TOP PAGE
$('.back-to-top').on('click',(function(){
	$("html, body").animate({ scrollTop: 0 }, "fast");
}));


// HANDLE LOADING PAGES IN FOOTER LINKS
$('.footer-links').on('click',(function(){
	var footerLinks = $(this).data('footer-links');

	$("html, body").animate({ scrollTop: 0 }, 0);

// LOAD PAGES FROM FOOTER
	$('#main').load('template/'+footerLinks+'.html', function() {
    $.getScript('js/pages/'+footerLinks+'.js');
	});
	}
));
