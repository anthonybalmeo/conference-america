function setCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

// HANDLE LOADING PAGES IN CLIENT CORNER
$('.cc-links li .nav-link-a').on('click', function() {
  var navLink = $(this).parent().data('cc-link');
  var navId = $(this).parent().data('id');
	if (navLink == 'my-account' || navLink == 'meeting-controls' || navLink == 'resources') {
    if ($(this).parent().hasClass('nav-sublinks')) {
      $(this).parent().toggleClass('open');
      setCookie(navId, $(this).parent().hasClass('open'), 1);
    }
	}
  else {
    openPage('client-corner/'+navLink);
  }
});

// TOGGLES SUB-NAVIGATION WITHIN CLIENT CORNER AND LOADS SUBLINKS
$('.cc-sublinks li a').on('click', function() {
	var ccLinks = $(this).data('cc-links');
	if(ccLinks=='reset-passcode') openModal('default','reset-passcode');
	else {
		$("html, body").animate({ scrollTop: 0 }, 0);
		$('#main').load('template/client-corner/'+ccLinks+'.html', function() {
	    $.getScript('js/pages/client-corner/'+ccLinks+'.js');
		});
	}
	closeClientCorner();
});

// LOGIN FUNCTIONALITY. TOGGLES SIGNIN/SIGNOUT VIEW
$( "#login" ).submit(function( event ) {
  event.preventDefault();
	$('.cc-links').removeClass('remove');
	$('.logout-container').removeClass('remove');
	$('.login-container').addClass('remove');
	$('.right-nav').addClass('signed-in');
	$('.users-icon').addClass('signed-in');
});

//SIGNOUT CASE
$('#signout').on('click', function(){
	$('.cc-links').addClass('remove');
	$('.log-state').removeClass('remove');
	$('.logout-container').addClass('remove');
	$('.login-container').removeClass('remove');
	$('.right-nav').removeClass('signed-in');
	$('.users-icon').removeClass('signed-in');
});


$('#main').on('click',(function(){
	$('body').removeClass('cc-menu');
}));

$('#modal').on('click',(function(){
	$('body').removeClass('cc-menu');
	if($(this).hasClass('cc-modal')) closeModal();
}));
