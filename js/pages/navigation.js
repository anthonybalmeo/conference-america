// HANDLES NAVIGATION LINKING (SERVICES/DOWNLOADS/CONTACT/ABOUT US)
	$('.navigation-links').click(function(){
		removeNavActive();
		$(this).addClass('active');
		var navLink = $(this).data('nav-link');
		if (navLink!='contact') {
			$('#main').load('template/'+navLink+'.html', function() {
		    $.getScript('js/pages/'+navLink+'.js');
			});
			closeNav();
			window.scrollTo(0,0);
		}
	});

// HANDLES NAVIGATION LINKING FOR MOBILE (SERVICES/DOWNLOADS/CONTACT/ABOUT US)
	$('.subnav-links').click(function(){
		$('.subnav-links').removeClass('active');
		$(this).addClass('active');
		handleActiveNavigation('navigation-links', 'contact');
		var navLink = $(this).data('nav-link');
		if (navLink=='chat') {
			openChat();
		}
		else {
			$('#main').load('template/'+navLink+'.html', function() {
		    $.getScript('js/pages/'+navLink+'.js');
			});
			closeNav();
			window.scrollTo(0,0);
		}
	});

// HANDLES SUBNAVIGATION LINKING MOBILE
	$('.subnav-mobile li').click(function(){
		$('.subnav-mobile li').removeClass('active');
		$(this).addClass('active');
		var navLink = $(this).data('nav-link');
		if (navLink=='chat') {
			openChat();
		}
		else {
			$('#main').load('template/'+navLink+'.html', function() {
		    $.getScript('js/pages/'+navLink+'.js');
			});
			closeNav();
			window.scrollTo(0,0);
		}
	});

	// HANDLES SUBNAVIGATION FOR CONTACT HOVERING
	$('.contact-nav, .subnav').hover(function(){
		 $('.subnav, .contact-nav').toggleClass('hovered');
	});
	$('.contact-nav').click(function(){
		 $(this).toggleClass('dropdown');
	});
