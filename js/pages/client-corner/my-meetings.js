/***********************************************************
INJECTING MEETING DATA INTO HTML
***********************************************************/
var meetingID = null;

/* ADD MEETINGS LIST */
var source   = $("#meeting-template").html();
var template = Handlebars.compile(source);
var temp = { meetings: meetingsArray };
var meetingsArg = template(temp);
$("#render-meeting").html(meetingsArg);

/***********************************************************
CLICK HANDLER TO LINK A CERTAIN MEETING
WITH ITS MEETING HISTORY INFO DATA
***********************************************************/
$('#render-meeting').on('click', '[data-meeting-link=handler]', function(){
  meetingID = $(this).data('meeting-id');
  var meetingStatus = $(this).data('meeting-status').toLowerCase();

  if(meetingStatus === 'active') {
    $('#main').load('template/client-corner/meeting-active.html', function() {
  		$.getScript('js/pages/client-corner/meeting-active.js', function() {
        return meetingID;
      });
  	});
  }

  else {
    $('#main').load('template/client-corner/meeting-history.html', function() {
  		$.getScript('js/pages/client-corner/meeting-history.js', function() {
        return meetingID;
      });
  	});
  }
});

/***********************************************************
LINKS TO SCHEDULE MEETING
***********************************************************/
$('#schedule-meeting').on('click',function(){
  $('#main').load('template/client-corner/schedule-meeting.html', function() {
		$.getScript('js/pages/client-corner/schedule-meeting.js');
	});
});
/***********************************************************
CALENDAR HANDLER
***********************************************************/
$(function() {
    var calIn = $( "#date-in" ).datepicker({
      disabled: false,
      showOn: "both",
      buttonImage: "img/icons/icon-calendar-gold.png",
      buttonImageOnly: true,
      buttonText: "Select Date",
      firstDay: 0,
      altFormat: 'mm/dd/yy',
      dateFormat: 'mm/dd/yy',

      beforeShow: function(input, inst) {
       var cal = inst.dpDiv;
       var top  = $(this).offset().top + $(this).outerHeight();
       var left = $(this).offset().left;
       setTimeout(function() {
          cal.css({
              'top' : top,
              'left': left
          });
       }, 10);
     }

    });

    var calOut = $( "#date-out" ).datepicker({
      disabled: false,
      showOn: "both",
      buttonImage: "img/icons/icon-calendar-gold.png",
      buttonImageOnly: true,
      buttonText: "Select Date",
      firstDay: 0,
      altFormat: 'mm/dd/yy',
      minDate: 1,
      dateFormat: 'mm/dd/yy',

      beforeShow: function(input, inst) {
       var cal = inst.dpDiv;
       var top  = $(this).offset().top + $(this).outerHeight();
       var left = $(this).offset().left;
       setTimeout(function() {
          cal.css({
              'top' : top,
              'left': left
          });
       }, 10);
     }


    });

  });
