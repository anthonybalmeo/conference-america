// FORM VALIDATION
validateForm('#edit-leader', 'client-corner/leader');

/***********************************************************
DETERMINES WHICH LEADER SELECTED TO
EDIT IS SELECTED FROM MY LEADERS PAGE
***********************************************************/
var editLeader = leadersArray.filter(function (el) {
  return el.employeeID === leaderID;
});
/***********************************************************
POPULATES FORM WITH WHAT LEADER WAS SELECTED
***********************************************************/
$('input[name="firstName"]').val(editLeader[0].firstName);
$('input[name="lastName"]').val(editLeader[0].lastName);
$('input[name="phone"]').val(editLeader[0].phone);
$('input[name="email"]').val(editLeader[0].email);
$('input[name="specialBillingID"]').val(editLeader[0].specialBillingID);
$('input[name="employeeID"]').val(editLeader[0].employeeID);
