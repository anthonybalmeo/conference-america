validateForm('#always-on-mp3-recording-request', 'always-on-mp3-recording-request');

/***********************************************************
CALENDAR HANDLER
***********************************************************/
$(function() {
    var calIn = $( "#date-of-call" ).datepicker({
      disabled: false,
      showOn: "both",
      buttonImage: "img/icons/icon-calendar-teal.png",
      buttonImageOnly: true,
      buttonText: "Select Date",
      altFormat: 'mm/dd/yy',
      dateFormat: 'mm/dd/yy',

      beforeShow: function(input, inst) {
       var cal = inst.dpDiv;
       var top  = $(this).offset().top + $(this).outerHeight();
       var left = $(this).offset().left;
       setTimeout(function() {
          cal.css({
              'top' : top,
              'left': left
          });
       }, 10);
     }

    });
  });
