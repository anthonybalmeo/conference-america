// CANCEL MEETING
$( '#invite-participants-form' ).submit(function( event ) {
  event.preventDefault();
  $('#main').load('template/client-corner/meeting-active.html', function() {
    $.getScript('js/pages/client-corner/meeting-active.js', function() {
      return meetingID;
    });
  });
  openModal('default','participants-invite');
});

$('.cancel-link').on('click', function(){
  $('#main').load('template/client-corner/meeting-active.html', function() {
    $.getScript('js/pages/client-corner/meeting-active.js', function() {
      return meetingID;
    });
  });
});
