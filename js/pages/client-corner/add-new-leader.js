// FORM VALIDATION
$( '#add-new-leader-form' ).submit(function( event ) {
  event.preventDefault();

  //GRABS FORM AND ADDS A NEW LEADER
  var formData = {};
  $.each($(this).serializeArray(), function (i, field) { formData[field.name] = field.value || ""; });
  formData.employeeID =  Math.floor((Math.random() * 100) + 1);
  leadersArray.push(formData);
  validateForm('#add-new-leader', 'add-leader');
});


/***********************************************************
CALENDAR HANDLER
***********************************************************/
$(function() {
    var calIn = $( "#date-in" ).datepicker({
      disabled: false,
      showOn: "both",
      buttonImage: "img/icons/icon-calendar-teal.png",
      buttonImageOnly: true,
      buttonText: "Select Date",
      altFormat: 'mm/dd/yy',
      dateFormat: 'mm/dd/yy',

      beforeShow: function(input, inst) {
       var cal = inst.dpDiv;
       var top  = $(this).offset().top + $(this).outerHeight();
       var left = $(this).offset().left;
       setTimeout(function() {
          cal.css({
              'top' : top,
              'left': left
          });
       }, 10);
     }

    });
  });
