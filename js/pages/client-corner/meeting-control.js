$('body').addClass('hide-elements');

/***********************************************************
HANDLE TAB NAVIGATION
- PARTICIPANTS
- Q&A
- VIEW ALL
***********************************************************/
$('.tab-nav li').on('click', function(){
  var navTab = $(this).data('nav-tab');
  $('.tab-nav li').removeClass('active');
  $(this).addClass('active');

   // $('.tab-nav-info-block').removeClass('active');
   $('.meeting-control-columns').removeClass('active');

  switch (navTab) {
	case 1:
		$('.meeting-control-columns[data-nav-meeting-control-block="'+navTab+'"]').addClass('active');
    $('.meeting-control-columns').removeClass('remove four six');
    $('.meeting-control-columns.participants').addClass('six');
    $('.meeting-control-columns.qa').addClass('remove');
    $('.meeting-control-columns.view-all').addClass('six');
		break;
	case 2:

  $('.meeting-control-columns[data-nav-meeting-control-block="'+navTab+'"]').addClass('active');
    $('.meeting-control-columns').removeClass('remove four six');
    $('.meeting-control-columns.participants').addClass('remove');
    $('.meeting-control-columns.qa').addClass('six');
    $('.meeting-control-columns.view-all').addClass('six');
    break;
    case 3:
		$('.meeting-control-columns[data-nav-meeting-control-block="'+navTab+'"]').addClass('active');
  		$('.meeting-control-columns').removeClass('remove');
      $('.meeting-control-columns').removeClass('six');
      $('.meeting-control-columns').addClass('four');
      break;
	default:
    $('.meeting-control-columns').removeClass('active');
    $('.meeting-control-columns').removeClass('remove');
    $('.meeting-control-columns').removeClass('six');
    $('.meeting-control-columns').addClass('four');
}
});

$( '#message-form' ).submit(function( event ) {
  event.preventDefault();
  var message = $('#messages').text();
});


var meetingControlListQA = meetingControlsArray.filter(function (el) {
  return el.type === 'qa';
});
var meetingControlListParticipants = meetingControlsArray.filter(function (el) {
  return el.type === 'participants';
});

/* MEETING CONTROL LIST INFO */
var sourceMeetingControlListInfoParticipants   = $("#meeting-control-list-participants-template").html();
var templateMeetingControlListInfoParticipants = Handlebars.compile(sourceMeetingControlListInfoParticipants);
var tempmeetingControlListInfoParticipants = { meetingControlListParticipants: meetingControlListParticipants };
var meetingHistorymeetingControlListArgParticipants = templateMeetingControlListInfoParticipants(tempmeetingControlListInfoParticipants);
$("#render-meeting-control-list-participants-info").html(meetingHistorymeetingControlListArgParticipants);

/* MEETING CONTROL LIST INFO */
var sourceMeetingControlListInfoQA   = $("#meeting-control-list-qa-template").html();
var templateMeetingControlListInfoQA = Handlebars.compile(sourceMeetingControlListInfoQA);
var tempmeetingControlListInfoQA = { meetingControlListQA: meetingControlListQA };
var meetingHistorymeetingControlListArgQA = templateMeetingControlListInfoQA(tempmeetingControlListInfoQA);
$("#render-meeting-control-list-qa-info").html(meetingHistorymeetingControlListArgQA);
