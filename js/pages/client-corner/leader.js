$('#add-new-leader-link').on("click", function(){
  openPage('client-corner/'+$(this).data('pages'));
});


/***********************************************************
INJECTING LEADERS DATA INTO HTML
***********************************************************/
var source   = $("#leaders-template").html();
var template = Handlebars.compile(source);
var temp = { leaders: leadersArray };
var leadersArg = template(temp);
$("#render-leaders").html(leadersArg);

/***********************************************************
CLICK HANDLER TO LINK A CERTAIN LEADER
TO EDIT INFO
***********************************************************/
$('#render-leaders').on('click', '[data-pages=edit-leader]', function(){
  leaderID = $(this).data('leader-id');
  $('#main').load('template/client-corner/edit-leader.html', function() {
		$.getScript('js/pages/client-corner/edit-leader.js', function() {
      return leaderID;
    });
	});
});


/***********************************************************
CLICK HANDLER TO MANAGE ON/OFF ON LEADERS + MODAL
***********************************************************/
var self = null;

$('.on-off-leader-check input[type="checkbox"]').on('click', function(){
  self = this;

  if(!$(this).prop('checked')) {
    openModal('default','turn-off-leader');

    $('.modal-page').removeClass('active');
    $('.modal-page[data-page-num=1]').addClass('active');

    $( '#turn-off-leader-form' ).submit(function( event ) {
      event.preventDefault();
      response = $('input[type=radio]:checked').attr('value');
      if(response === 'yes') {
        $('.modal-page').removeClass('active');
        $('.modal-page[data-page-num=2]').addClass('active');
        $(self).prop( "checked", false );
      }
      else {
        $(self).prop( "checked", true );
        closeModal();
      }

    });

  }
});
