/***********************************************************
HANDLE TAB NAVIGATION
- INFORMATION
- RSVP
***********************************************************/
$('.tab-nav li').on('click', function(){
  var navTab = $(this).data('nav-tab');
  $('.tab-nav li').removeClass('active');
  $(this).addClass('active');

  $('.tab-nav-info-block').removeClass('active');
  $('.tab-nav-info-block[data-nav-info-block="'+navTab+'"]').addClass('active');
});

/***********************************************************
DETERMINES WHICH MEETING HISTORY
IS SELECTED FROM MY MEETINGS PAGE
***********************************************************/
var meeting = meetingsArray.filter(function (el) {
  return el.id === meetingID;
});

/***********************************************************
INJECTING MEETING ACTIVE DATA INTO HTML
***********************************************************/
var sourceInfo   = $("#meeting-active-info-template").html();
var templateInfo = Handlebars.compile(sourceInfo);
var tempInfo = meeting[0];
var meetingActiveInfoArg = templateInfo(tempInfo);
$("#render-meeting-active-info").html(meetingActiveInfoArg);

// JOIN INFO
var sourceJoinInfo   = $("#join-info-template").html();
var templateJoinInfo = Handlebars.compile(sourceJoinInfo);
var tempJoinInfo = meeting[0].joinInformation;
var meetingActiveJoinInfoArg = templateJoinInfo(tempJoinInfo);
$("#render-join-info").html(meetingActiveJoinInfoArg);


/* ENHANCEMENT LIST INFO */
var sourceEnhancementListInfo   = $("#enhancement-info-template").html();
var templateEnhancementListInfo = Handlebars.compile(sourceEnhancementListInfo);
var tempEnhancementListInfo = { enhancementList: meeting[0].joinInformation.enhancements };
var meetingActiveEnhancementListArg = templateEnhancementListInfo(tempEnhancementListInfo);
$("#render-enhancement-info").html(meetingActiveEnhancementListArg);
/***********************************************************
INJECTING RSVP DATA INTO HTML
***********************************************************/
/* RSVP INFO */
var sourceRSVP   = $("#meeting-active-rsvp-template").html();
var templateRSVP = Handlebars.compile(sourceRSVP);
var tempRSVP = meeting[0];
var meetingActiveRSVPArg = templateRSVP(tempRSVP);
$("#render-meeting-active-rsvp").html(meetingActiveRSVPArg);

/* RSVP LIST INFO */
var sourceRSVPListInfo   = $("#meeting-active-rsvpList-template").html();
var templateRSVPListInfo = Handlebars.compile(sourceRSVPListInfo);
var tempRSVPListInfo = { rsvpList: meeting[0].rsvp.rsvpList };
var meetingActiveRSVPListArg = templateRSVPListInfo(tempRSVPListInfo);
$("#render-meeting-active-rsvpList").html(meetingActiveRSVPListArg);





// CANCEL MEETING
$('.cancel-meeting').on('click', function(){
  openModal('default','cancel-scheduled-meeting');
});
$( '#cancel-scheduled-meeting-form' ).submit(function( event ) {
  event.preventDefault();
  var response = $('input[type=radio]:checked').val();
  if(response==='no') {
    closeModal();
  } else {
    meetingsArray[meeting[0].id-1].status = 'Canceled';
    closeModal();
    $('#main').load('template/client-corner/my-meetings.html', function() {
  		$.getScript('js/pages/client-corner/my-meetings.js');
  	});
  }
});

 // openModal('default','participants-invite');
$('.invite-participants').on('click', function(){
  $('#main').load('template/client-corner/invite-participants.html', function() {
    $.getScript('js/pages/client-corner/invite-participants.js', function() {
      return meetingID;
    });
  });
});
