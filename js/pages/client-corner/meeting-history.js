/***********************************************************
HANDLE TAB NAVIGATION
- INFORMATION
- RSVP
- PARTICIPANTS
***********************************************************/
$('.tab-nav li').on('click', function(){
  var navTab = $(this).data('nav-tab');
  $('.tab-nav li').removeClass('active');
  $(this).addClass('active');

  $('.tab-nav-info-block').removeClass('active');
  $('.tab-nav-info-block[data-nav-info-block="'+navTab+'"]').addClass('active');
});

/***********************************************************
DETERMINES WHICH MEETING HISTORY
IS SELECTED FROM MY MEETINGS PAGE
***********************************************************/
var meeting = meetingsArray.filter(function (el) {
  return el.id === meetingID;
});

/***********************************************************
INJECTING MEETING HISTORY DATA INTO HTML
***********************************************************/
var sourceInfo   = $("#meeting-history-info-template").html();
var templateInfo = Handlebars.compile(sourceInfo);
var tempInfo = meeting[0];
var meetingHistoryInfoArg = templateInfo(tempInfo);
$("#render-meeting-history-info").html(meetingHistoryInfoArg);

/***********************************************************
INJECTING RSVP DATA INTO HTML
***********************************************************/
/* RSVP INFO */
var sourceRSVP   = $("#meeting-history-rsvp-template").html();
var templateRSVP = Handlebars.compile(sourceRSVP);
var tempRSVP = meeting[0];
var meetingHistoryRSVPArg = templateRSVP(tempRSVP);
$("#render-meeting-history-rsvp").html(meetingHistoryRSVPArg);

/* RSVP LIST INFO */
var sourceRSVPListInfo   = $("#meeting-history-rsvpList-template").html();
var templateRSVPListInfo = Handlebars.compile(sourceRSVPListInfo);
var tempRSVPListInfo = { rsvpList: meeting[0].rsvp.rsvpList };
var meetingHistoryRSVPListArg = templateRSVPListInfo(tempRSVPListInfo);
$("#render-meeting-history-rsvpList").html(meetingHistoryRSVPListArg);


/***********************************************************
INJECTING AUDIO CONFERENCE PARTICIPANTS DATA INTO HTML
***********************************************************/
var sourceParticipants   = $("#meeting-history-participants-template").html();
var templateParticipants = Handlebars.compile(sourceParticipants);
var tempParticipants = { participants: meeting[0].participants };
var meetingHistoryparticipantsArg = templateParticipants(tempParticipants);
$("#render-meeting-history-participants").html(meetingHistoryparticipantsArg);
