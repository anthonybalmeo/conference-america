var operatorHandledPageNumber = 0;
var alwaysOnPageNumber = 0;

function handleScheduleSections(pagetype, pageNum){
  $('.section').removeClass('active');
  $('.section[data-'+pagetype+'-schedule-page='+pageNum+']').addClass('active');
}

$('.operator-handled').on('click',function(){
  operatorHandledPageNumber++;
  handleScheduleSections('operator-handled', operatorHandledPageNumber);
  window.scrollTo(0,0);
});

$('.always-on').on('click',function(){
  alwaysOnPageNumber++;
  handleScheduleSections('always-on', alwaysOnPageNumber);
  window.scrollTo(0,0);
});


$('.section[data-schedule-meeting-type="operatorHandled"] .next-button').on('click', function(){
  operatorHandledPageNumber++;
  handleScheduleSections('operator-handled', operatorHandledPageNumber);
  window.scrollTo(0,0);
});

$('.content-box-list-row[data-meeting-link="handler"]').on('click', function(){
  alwaysOnPageNumber++;
  handleScheduleSections('always-on', alwaysOnPageNumber);
  window.scrollTo(0,0);
});

$('.go-back').on('click', function(){
  alwaysOnPageNumber--;
  handleScheduleSections('always-on', alwaysOnPageNumber);
  window.scrollTo(0,0);
});

$( 'form' ).submit(function( event ) {
  event.preventDefault();
    openSuccessPage('schedule-meeting');
});

/***********************************************************
CALENDAR HANDLER
***********************************************************/
$(function() {
    var meetingDate = $( "#meeting-date-operator-handled" ).datepicker({
      disabled: false,
      showOn: "both",
      buttonImage: "img/icons/icon-calendar-teal.png",
      buttonImageOnly: true,
      buttonText: "Select Date",
      altFormat: 'mm/dd/yy',
      dateFormat: 'mm/dd/yy',

      beforeShow: function(input, inst) {
       var cal = inst.dpDiv;
       var top  = $(this).offset().top + $(this).outerHeight();
       var left = $(this).offset().left;
       setTimeout(function() {
          cal.css({
              'top' : top,
              'left': left
          });
       }, 10);
     }
    });


    var specialRequirementsDate = $( "#special-requirements-date" ).datepicker({
      disabled: false,
      showOn: "both",
      buttonImage: "img/icons/icon-calendar-teal.png",
      buttonImageOnly: true,
      buttonText: "Select Date",
      altFormat: 'mm/dd/yy',
      dateFormat: 'mm/dd/yy',

      beforeShow: function(input, inst) {
       var cal = inst.dpDiv;
       var top  = $(this).offset().top + $(this).outerHeight();
       var left = $(this).offset().left;
       setTimeout(function() {
          cal.css({
              'top' : top,
              'left': left
          });
       }, 10);
     }
    });

    var meetingDateAlwaysOn = $( "#meeting-date-always-on" ).datepicker({
      disabled: false,
      showOn: "both",
      buttonImage: "img/icons/icon-calendar-teal.png",
      buttonImageOnly: true,
      buttonText: "Select Date",
      altFormat: 'mm/dd/yy',
      dateFormat: 'mm/dd/yy',

      beforeShow: function(input, inst) {
       var cal = inst.dpDiv;
       var top  = $(this).offset().top + $(this).outerHeight();
       var left = $(this).offset().left;
       setTimeout(function() {
          cal.css({
              'top' : top,
              'left': left
          });
       }, 10);
     }
    });
  });
