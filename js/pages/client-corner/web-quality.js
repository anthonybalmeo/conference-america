// NAVIGATE FORWARD TO OTHER VIEWS/SECTIONS
handleCtaContinue('web-quality');

$('.home-icon').on('click', function(){
  openPage('client-corner/web-quality');
})


var viewIssueBoxText = false;

// VIEW ISSUE SELECTION
$('.view-issue').on('click', function() {
  $('.issues-box').toggleClass('display');
  viewIssueBoxText = viewIssueBoxText ? false : true;
  $(this).children('span').text((viewIssueBoxText ? 'Hide' : 'View'));
});
/***********************************************************
CALENDAR HANDLER
***********************************************************/
$(function() {
    var calIn = $( "#date-in" ).datepicker({
      disabled: false,
      showOn: "both",
      buttonImage: "img/icons/icon-calendar-gold.png",
      buttonImageOnly: true,
      buttonText: "Select Date",
      altFormat: 'mm/dd/yy',
      dateFormat: 'mm/dd/yy',

      beforeShow: function(input, inst) {
       var cal = inst.dpDiv;
       var top  = $(this).offset().top + $(this).outerHeight();
       var left = $(this).offset().left;
       setTimeout(function() {
          cal.css({
              'top' : top,
              'left': left
          });
       }, 10);
     }

    });

    var calOut = $( "#date-out" ).datepicker({
      disabled: false,
      showOn: "both",
      buttonImage: "img/icons/icon-calendar-gold.png",
      buttonImageOnly: true,
      buttonText: "Select Date",
      altFormat: 'mm/dd/yy',
      dateFormat: 'mm/dd/yy',

      beforeShow: function(input, inst) {
       var cal = inst.dpDiv;
       var top  = $(this).offset().top + $(this).outerHeight();
       var left = $(this).offset().left;
       setTimeout(function() {
          cal.css({
              'top' : top,
              'left': left
          });
       }, 10);
     }

    });

  });
