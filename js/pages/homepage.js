// HANDLE PAGE LINKING ON HOME PAGE
$('.page-linking').on('click', function(){
	openPage($(this).data('pages'));
});


/*
	HANDLES LINKING TO SERVICES PAGE (AlwaysOn Conferencing & High Definition Voice )
	THIS CAN BE DONE ONCE YOU CREATE THE ROUTING. FOR EX (www.conferenceamerica.com/#alwaysOn  && www.conferenceamerica.com/#hdv )
	DONE THIS WAY JUST TO SHOWCASE THE INTERACTION FROM CLICKING FROM HOMEPAGE TO SERVICES APGE
*/
$('.link-to').click('on',function(){
	var linkTo = $(this).data('link-to-page');
	$('#main').load('template/services.html', function() {
			$.getScript('js/pages/services.js');
			var headerHeight = $('#header-container').height();
			$("html, body").animate({ scrollTop: $('#'+linkTo).offset().top - headerHeight }, 0);
		});
});


