var leadersArray = [];
var ccManagerArray = [];
var meetingsArray = [];
var meetingControlsArray = [];

// LOAD LEADERS SERVICE JSON OBJECT
$.ajax({
  url: "js/services/leaders.json",
     //force to handle it as text
  dataType: "text",
  success: function (data) {

    var json = $.parseJSON(data);

    var arr = [];
    $.each(json, function (i, jsonObjectList) {
    for (var index = 0; index < jsonObjectList.length;index++) {
          leadersArray.push(jsonObjectList[index]);
          }
     });

     arr = leadersArray;
   }
});

// LOAD EVENTS/MEETINGS SERVICE JSON OBJECT
$.ajax({
  url: "js/services/ccmanager.json",
     //force to handle it as text
  dataType: "text",
  success: function (data) {

    var json = $.parseJSON(data);

    var arr = [];
    $.each(json, function (i, jsonObjectList) {
    for (var index = 0; index < jsonObjectList.length;index++) {
          ccManagerArray.push(jsonObjectList[index]);
          }
     });

     arr = ccManagerArray;
   }
});


// LOAD MEETINGS SERVICE JSON OBJECT
$.ajax({
  url: "js/services/meetings.json",
     //force to handle it as text
  dataType: "text",
  success: function (data) {

    var json = $.parseJSON(data);

    var arr = [];
    $.each(json, function (i, jsonObjectList) {
    for (var index = 0; index < jsonObjectList.length;index++) {
          meetingsArray.push(jsonObjectList[index]);
          }
     });

     arr = meetingsArray;
   }
});


// LOAD MEETINGS SERVICE JSON OBJECT
$.ajax({
  url: "js/services/meeting-controls.json",
     //force to handle it as text
  dataType: "text",
  success: function (data) {

    var json = $.parseJSON(data);

    var arr = [];
    $.each(json, function (i, jsonObjectList) {
    for (var index = 0; index < jsonObjectList.length;index++) {
          meetingControlsArray.push(jsonObjectList[index]);
          }
     });

     arr = meetingControlsArray;
   }
});
