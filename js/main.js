/***** FUNCTIONS *****/

function openModal(type, modalSectionType) {
	if(type !== 'default') {
		$('#modal').addClass(type);
		$('.'+type).addClass('open');
	}
	else {
		$('#modal').removeClass('cc-modal');
		$('#modal').addClass('open');
		$('.modal-container').removeClass('view');
		$('.modal-container[data-modal-container-type="'+modalSectionType+'"]').addClass('view');
	}
}

function closeModal() {
	$('#modal').removeClass('open');
	$('#modal').removeClass('cc-modal');
}

// OPEN PRINT BOX
function printPage() {
	window.print();
}

// REMOVE ACTIVE NAVIGATION STATE
function removeNavActive(){
	$('.navigation-links').removeClass('active');
	$('.subnav-links').removeClass('active');
}

// TOGGLE OPEN CHAT. OPENS CHAT IN A NEW WINDOW
function openChat() {
		window.open("template/chat.html", "_blank", "scrollbars=1,resizable=1,height=466,width=500,left=0,titlebar=0,location=0");
}

// TOGGLE NAVIGATION OPEN
function navOpen() {
	$('body').toggleClass('nav-open');
	if($('body').hasClass('cc-menu')) $('body').removeClass('cc-menu');
}

// CLOSE NAV  - USED TO CLOSE NAVIGATION WHEN IN MOBILE STATE
function closeNav() {
	$('body').removeClass('nav-open');
	$('#modal').removeClass('open');
}


// CLOSE NAV  - USED TO CLOSE NAVIGATION WHEN IN MOBILE STATE
function closeClientCorner() {
	$('body').removeClass('cc-menu');
	$('#modal').removeClass('cc-modal');
}


// OPEN CREDIT CARD PAGE
function openCreditCard(){
	$('#main').load('template/credit-card.html', function() {
		$.getScript('js/pages/credit-card.js');
	});
}

// OPEN INVOICE PAGE
function openInvoice(){
	$('#main').load('template/invoice.html', function() {
		$.getScript('js/pages/invoice.js');
	});
}

// OPEN SPANISH SERVICES PAGE
function openSpanishServices(){
	$('#main').load('template/spanish-services.html', function() {
		$.getScript('js/pages/spanish-services.js');
	});
}

// OPEN SUCCESS PAGE
function openSuccessPage(value) {
	if(value=='create-account' || value == 'contact' || value == 'add-leader' || value == 'schedule-meeting' || value == 'always-on-mp3-recording-request') {
		$('#main').load('template/success-page.html', function() {
			$.getScript('js/pages/success-page.js');
				$('.success-page-type[data-success-page-type="'+value+'"]').addClass('view');
		});
	}
	else {
		openPage(value);
	}


	window.scrollTo(0,0);

}

// HANDLE NAVIGATION ACTIVE STATE
function handleActiveNavigation(navType, pageNav) {
	var navigationType = navType;
	var navigation = $('.'+navigationType+'[data-nav-link="'+pageNav+'"]');
	$(navigation).addClass('active');
	closeNav();
	window.scrollTo(0,0);
}

/*
	OPEN RESPECTIVE PAGES WHEN SELECING FROM NAVIGATION. UPON LOADING THE PAGE, I CREATED JAVASCRIPT
	FILES FOR EACH COMPONENT LOADED. LOADING A TEMPLATE/COMPONENT LOADS IN THE JS FILE RESPECTIVE TO THE
	PAGE.
*/
function openPage(page){
	var navLink = page;
	removeNavActive();
	handleActiveNavigation('navigation-links', navLink);
	if(navLink == 'create-account' || navLink == 'credit-card' || navLink == 'invoice'){
		handleActiveNavigation('navigation-links', 'contact');
		handleActiveNavigation('subnav-links', 'create-account');
	}
	$('#main').load('template/'+navLink+'.html', function() {
		$.getScript('js/pages/'+navLink+'.js');
	});
}


/*
	VALIDATE FORM - CREDIT CARD/INVOICE
*/

function validateForm(form, successPageType) {
	errors = false;
	// HANDLES SUCCESS PAGE ON FORM SUBMIT
	$( form ).submit(function( event ) {
	  event.preventDefault();
	  if(!errors)
	    openSuccessPage(successPageType);
	});


	// VALIDATES FORM WHEN MOVING FROM DIFFERENT INPUT
	$('.validate').blur(function() {
	  var name = $(this).attr("name");
	  var validateThis = $(this);
	  // DETERMINES IF NUMERICAL VALUE
	  var numberRegex = /^\d+$/;
	  // DETERMINES IF EMAIL VALUE
	  var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  var clearError = function() { $( validateThis ).siblings( ".error-message" ).text(''); };
	  var setError = function(errorValue) { $( validateThis ).siblings( ".error-message" ).text(validateThis.data(errorValue)); };

	  if ($(this).val()==='') {
	    $(this).addClass('invalid');
	    $( validateThis ).siblings( ".error-message" ).text(validateThis.data('error-blank'));
	    error = false;
	  }
	  else {
	    $(this).removeClass('invalid');
	    clearError();
	    error = false;
	    if(name == 'creditCard'){
	      if (!validateThis.val().match(numberRegex)) {
	        setError('error-credit-card');
	        validateThis.addClass('invalid');
	        error = true;
	      }
	      else {
	        clearError();
	        validateThis.removeClass('invalid');
	        error = false;
	      }
	    }

	    // VALIDATE NUMBER VALUE FOR ZIPCODE
	    if(name == 'zipcode'){
	      if (!validateThis.val().match(numberRegex)){
	        setError('error-zip');
	        validateThis.addClass('invalid');
	        error = true;
	      }
	      else {
	        validateThis.removeClass('invalid');
	        clearError();
	        error = false;
	      }
	    }

	    // VALIDATE IF EMAIL
	    if(name == 'email'){
	      if (!validateThis.val().match(emailRegex)){
	        setError('error-email');
	        validateThis.addClass('invalid');
	        error = true;
	      }
	      else {
	        validateThis.removeClass('invalid');
	        clearError();
	        error = false;
	      }
	    }
	  }
	});
}

// IF A CTA CONTINUE EXISTS ON A PAGE, THIS FUNCTION WILL PROGRESS THROUGH THE FOLLOWING SECTIONS
function handleCtaContinue(page) {
	$('.cta-continue').on('click',function(){
	  $('#' + page + ' .section').removeClass('active');
	  $('#' + page + ' .section-'+$(this).data('option')).addClass('active');
	})
}

/***** DOCUMENT READY  ******/

$(document).ready(function(){

	/*
	LOAD IN COMPONENTS ON DOCUMENT READY. COMPARTMENTALIZED EACH SECTION TO MODULARIZE THE PAGE.
	WILL BE EASIER TO LOCATE EACH SECTION WHEN WORKING WITH THE CODE.

	COMPONENTS:
	- HEADER
	- NAVIGATION
	- CLIENT CORNER
	- FOOTER
	- MAIN ( MAIN SECTION WHICH SWITCHES OUT THE PAGES (EX. SERVICE/CREDIT CARD/CONTACT US/ ETC.) )
	- MODAL
	*/


	$('#header').load('template/header.html', function() {
    $.getScript('js/header.js');
	});
	$('#navigation').load('template/navigation.html', function() {
		$.getScript('js/pages/navigation.js');
	});
	$('#client-corner').load('template/client-corner.html', function() {
    $.getScript('js/pages/client-corner.js');
	});
	$('#footer').load('template/footer.html', function() {
    $.getScript('js/pages/footer.js');
	});
	$('#main').load('template/homepage.html', function() {
    $.getScript('js/pages/homepage.js');
	});
	$('#modal').load('template/modal.html', function() {
    $.getScript('js/pages/modal.js');
	});

});

/***** WINDOW LOAD  ******/

$(window).load(function(){
});

/***** WINDOW RESIZE  ******/

$(window).resize(function(){

});

/***** WINDOW SCROLL  ******/
$( window ).scroll(function() {
  if ($(window).scrollTop() > 80){
  	$('body').addClass('body-scroll');
  }
  else {
  	$('body').removeClass('body-scroll');
  }
});
