// GO TO HOME WHEN CLICKED
$('#home').on('click',(function(){
		removeNavActive();
		$('#main').load('template/homepage.html', function() {
	    $.getScript('js/pages/homepage.js');
		});
		closeNav();
	}));

// TOGGLE OPEN SEARCH BAR
	$('.search-icon').on('click',(function(){
		if($('.search-section').hasClass('visible'))
			$('.search-section').removeClass('visible');
		else
			$('.search-section').addClass('visible');
	}));
	$('#search-nav').on('click',(function(){
		if($('.search-section').hasClass('visible'))
			$('.search-section').removeClass('visible');
		else
			$('.search-section').addClass('visible');
	}));

	// TOGGLE OPEN NAVIGATION - MOBILE
	$('#hamburger').on('click',(function(){
		navOpen();
	}));

	// OPEN CHAT
	$('.chat-icon, .chat').on('click',(function(){
		openChat();
	}));

	// OPEN CLIENT CORNER
	$('.client, .users-icon').on('click',(function(){
		$('body').toggleClass('cc-menu');
		openModal('cc-modal', '');
		closeNav();
	}));

	// EXIT CLIENT CORNER
	$('#client-corner .exit').on('click',(function(){
		$('body').removeClass('cc-menu');
		closeModal();
	}));

	// CASE WHEN USER CLICKS ANYWHERE IN THE BODY. CLOSES THE SEARCH BAR
	$('#main').on('click',(function(){
		$('.search-section').removeClass('visible');
	}));



/*****
 HANDLES HEADER AREA. MINIMIZES ON SCROLL
******/
$( window ).scroll(function() {
  if ($(window).scrollTop() > 80){
  	$('.ca').addClass('display-hide');
  	$('.ca').removeClass('display-show');
  	$('.ca-mobile').removeClass('display-hide');
  	$('.ca-mobile').addClass('display-show');
  	$('body').addClass('body-scroll');
  }
  else {
  	$('.ca').addClass('display-show');
  	$('.ca').removeClass('display-hide');
  	$('.ca-mobile').removeClass('display-show');
  	$('.ca-mobile').addClass('display-hide');
  	$('body').removeClass('body-scroll');
  }
});
